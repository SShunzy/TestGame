package be.thys.entity;

public interface IAttacking {
    int getPhysicalAttackPoints();
    int getMagicalAttackPoints();
    float getCriticalChance();
    float getAccuracy();
    void executeAction(IAction action, IAttackable target);
}
