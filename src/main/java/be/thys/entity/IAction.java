package be.thys.entity;

public interface IAction {
    int action(IAttacking attacking, IAttackable target);
}
