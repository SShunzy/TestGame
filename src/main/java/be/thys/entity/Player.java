package be.thys.entity;

import java.util.Random;

public class Player implements IAttackable{
    int defensePoints,AttackPoints;
    float criticalChance;
    double healthPoints, remainingHealthPoints, avoidingChance;

    @Override
    public double getHealthPoints() {
        return healthPoints;
    }

     @Override
     public double getRemainingHealthPoints(){
        return remainingHealthPoints;
     }

    @Override
    public int getDefensePoints() {
        return defensePoints;
    }

    @Override
    public double getAvoidingChance(){
        return avoidingChance;
    }

    @Override
    public void receiveDamage(double damagePoints) {
        Random random = new Random();
        if(random.nextDouble() > (100*getAvoidingChance())){
            double receivedDamage = damagePoints / defensePoints;
            remainingHealthPoints = remainingHealthPoints - receivedDamage;
            if (remainingHealthPoints <= 0.0) noHPRemaining();
        }
    }

    @Override
    public void noHPRemaining() {
        System.out.println("Game Over");
    }
}
