package be.thys.entity;

public interface IAttackable {
    double getHealthPoints();
    double getRemainingHealthPoints();
    int getDefensePoints();
    double getAvoidingChance();
    void receiveDamage(double damagePoints);

    void noHPRemaining();
}
